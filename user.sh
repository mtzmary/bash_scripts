#!/bin/bash
ROOT_UID=0
SUCCESS=0

# Run as root, of course. (this might not be necessary, because we have to run the script somehow with root anyway)
if [ "$UID" -ne "$ROOT_UID" ]
then
  echo "Se debe estar como root para ejecutar este script"
  exit $E_NOTROOT
fi  

file=$1

if [ "${file}X" = "X" ];
then
   echo "Debe indicar el archivo del listado con Nombres de usuario a ingresar..."
   exit 1
fi
# Del archivo con el listado de usuarios a ingresar:
# Este es el formato:
# ejemplo
#    |         |       |     |                 |         |        |
#    f1        f2      f3    f4                f5        f6       f7
#$f1 = username
#$f2 = password
#$f3 = User ID
#$f4 = Group ID (GID), este ID debe esixtir en /etc/group
#$f5 = User ID INFO
#$f6 = home directory
#$f7 =comand shell

crearUsuario(){
	#echo "----> Crear Usuario <----"
	eval user="$f1"
    eval pass="$f2"
    eval ID="$f3"
    eval group="$f4"
    eval INFO="$f5"
    eval home="$f6"
    eval shell="$f7"
	#echo "username 		  = ${username}"
	#echo "-------------------------"
	
	#-p, --password PASSWORD
    #Note: This option is not recommended because the password (or encrypted password) will be visible by users listing the processes.
    #You should make sure the password respects the system's password policy.
	
useradd -m -c $user -p $pass -u $ID -g $group -c $INFO -d $home -s $shell
 
	if [ $? -eq $SUCCESS ];
	then
		echo "Usuario [${user}] agregado correctamente..."
	else
		echo "Usuario [${user}] No se pudo agregar..."
	fi
}
	count=0
while IFS=: read -r f1 f2 f3 f4 f5 f6 f7
do
count=$(($count + 1))
		echo "|========================Usuario====[$count]=============|"
		echo -e "\t Username: [\e[34m${f1}\e[0m]"
		echo -e "\t password: [\e[34m${f2}\e[0m]"
		echo -e "\t User_ID: [\e[34m${f3}\e[0m]"
		echo -e "\t Group_ID: [\e[34m${f4}\e[0m]"
		echo -e "\t Full_Name: [\e[34m${f5}\e[0m]"
		echo -e "\t .Home./: [\e[34m${f6}\e[0m]"
		echo -e "\t ../Shell: [\e[34m${f7}\e[0m]"
		echo "|===================================================|"
		sleep 1s
crearUsuario "\${f1}"	
done < ${file}
exit 0
